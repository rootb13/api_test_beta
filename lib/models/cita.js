'use strict';

const Schwifty = require('schwifty');
const Joi = require('@hapi/joi');

module.exports = class Cita extends Schwifty.Model {

    static get tableName() {

        return 'citas';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().positive().min(1),
            paciente: Joi.number().integer().positive().min(1),
            medico: Joi.number().integer().positive().min(1),
            inicio: Joi.number().integer().positive().min(1),
            fin: Joi.number().integer().positive().min(1)
        });
    }
};
