'use strict';

exports.up = async (knex) => {

    await knex.schema.createTable('Usuarios', (t) => {

        t.increments('id').primary();
        t.string('nombre');
        t.string('apellido');
        t.string('correo').notNullable().unique();
        t.string('contraseña');
        t.string('rol');
    });
};

exports.down = async (knex) => {

    await knex.schema.dropTable('');
};
