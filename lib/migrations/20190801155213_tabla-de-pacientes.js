'use strict';

exports.up = async (knex) => {

    await knex.schema.createTable('Pacientes', (t) => {

        t.increments('id').primary();
        t.string('nombre');
        t.string('apellido');
        t.integer('telefono');
        t.string('correo').notNullable().unique();
        t.string('rfc');
    });
};

exports.down = async (knex) => {

    await knex.schema.dropTable('');
};
