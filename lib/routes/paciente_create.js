'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'post',
    path: '/pacientes',
    options: {
        validate: {
            payload: {
                nombre: Joi.string().min(3).max(35),
                apellido: Joi.string().min(3).max(45),
                telefono: Joi.number().integer().min(1000000000).max(9999999999),
                correo: Joi.string().min(10).max(50),
                rfc: Joi.string().min(10).max(13)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const paciente = request.payload;

        // pacientesConCorreoDuplicado es una lista [] de pacientes, puede estar vacia.
        const pacientesConCorreoDuplicado = await request.models()
            .Paciente
            .query()
            .where({ correo: paciente.correo });

        // Si la lista NO esta vacia
        if (pacientesConCorreoDuplicado.length > 0) {
            return h.response({ Error: 'Ya hay un paciente con ese correo' }).code(400);
        }

        const nuevoPaciente = await request.models()
            .Paciente
            .query()
            .insert(paciente);

        return nuevoPaciente;
    }
};
