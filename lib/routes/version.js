'use strict';

module.exports = {
    method: 'get',
    path: '/version',
    options: {
        handler: (request, h) => {

            const version = {
                author: 'Antonio'
            };

            return version;
        }
    }
};
