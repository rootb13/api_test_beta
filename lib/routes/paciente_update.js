'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'patch',
    path: '/pacientes/{paciente}',
    options: {
        validate: {
            params: {
                paciente: Joi.number().integer().positive().min(1)
            },
            payload: {
                nombre: Joi.string().min(3).max(35),
                apellido: Joi.string().min(3).max(45),
                telefono: Joi.number().integer().min(1000000000).max(9999999999),
                correo: Joi.string().min(10).max(50),
                rfc: Joi.string().min(10).max(13)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idP = request.params.paciente;

        const actualizacion = request.payload;

        const pacienteConIdP = await request.models()
            .Paciente
            .query()
            .where({ id: idP })
            .first();

        if (!pacienteConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `El paciente ${idP} no existe en la base de datos.`
            }).code(404);
        }

        await request.models()
            .Paciente
            .query()
            .where({ id: idP })
            .patch(actualizacion);

        return pacienteConIdP;
    }
};
