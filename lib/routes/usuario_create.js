'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'post',
    path: '/usuarios',
    options: {
        validate: {
            payload: {
                nombre: Joi.string().min(3).max(35),
                apellido: Joi.string().min(3).max(45),
                correo: Joi.string().min(10).max(50),
                contraseña: Joi.string().min(8).max(32),
                rol: Joi.string().min(5).max(15)
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const usuario = request.payload;

        // usuarioConEseCorreo es una lista [] de usuarios, puede estar vacia.
        const usuarioConEseCorreo = await request.models()
            .Usuario
            .query()
            .where({ correo: usuario.correo });

        // Si la lista NO esta vacia
        if (usuarioConEseCorreo.length > 0) {
            return h.response({ Error: 'Ya hay un usuario con ese correo' }).code(400);
        }

        const nuevoUsuario = await request.models()
            .Usuario
            .query()
            .insert(usuario);

        return { ...nuevoUsuario, contraseña: undefined };
    }
};
