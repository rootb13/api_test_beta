'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'delete',
    path: '/usuarios/{usuario}',
    options: {
        validate: {
            params: {
                usuario: Joi.number().integer().positive()
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idU = request.params.usuario;

        const usuarioConIdP = await request.models()
            .Usuario
            .query()
            .where({ id: idU })
            .first();

        if (!usuarioConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not found',
                message: `El usuario ${idU} no existe en la base de datos.`
            }).code(404);
        }

        const deleteUsuario = await request.models()
            .Usuario
            .query()
            .where({ id: idU })
            .delete();

        return `${deleteUsuario} ha sido eliminado`;
    }
};
