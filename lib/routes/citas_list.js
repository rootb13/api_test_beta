'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'get',
    path: '/citas',
    options: {
        validate: {
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const listaCitas = await request.models()
            .Cita
            .query();

        return listaCitas;
    }
};
