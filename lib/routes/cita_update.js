'use strict';

const Joi = require('@hapi/joi');

module.exports = {
    method: 'patch',
    path: '/citas/{cita}',
    options: {
        validate: {
            params: {
                cita: Joi.number().integer().positive()
            },
            payload: {
                paciente: Joi.number().integer().positive(),
                medico: Joi.number().integer().positive(),
                inicio: Joi.number().integer().positive(),
                fin: Joi.number().integer().positive()
            },
            headers: Joi.object({
                authorization: Joi.string().required().regex(new RegExp('^([Bb]earer )[\\w-.~+\/=]{1,}'))
            }).options({ allowUnknown: true })
        }
    },
    handler: async (request, h) => {

        const idP = request.params.cita;

        const actualizacion = request.payload;

        const citaConIdP = await request.models()
            .Cita
            .query()
            .where({ id: idP })
            .first();

        if (!citaConIdP) {
            return h.response({
                statusCode: 404,
                error: 'Not Found',
                message: `La cita ${idP} no existe en la base de datos.`
            }).code(404);
        }

        await request.models()
            .Cita
            .query()
            .where({ id: idP })
            .patch(actualizacion);

        const citaActualizada = await request.models()
            .Cita
            .query()
            .where({ id: idP })
            .first();

        return citaActualizada;
    }
};
