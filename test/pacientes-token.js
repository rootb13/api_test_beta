'use strict';

// Load modules

const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Familia de pruebas unitarias para manejar el token a [/pacientes/{paciente}] con header authorization Bearer.', () => {

    let server;

    it('[GET] /pacientes/{paciente} responde 400 cuando {paciente} es un id igual a 0.', async () => {

        server = await Server.deployment();

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/0'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando {paciente} es un id negativo.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/-11'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando no manda ningun header.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header vacio.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {}
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header authorization vacio.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: ''
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header authorization Bearer sin un jwt.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: 'Bearer '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header authorization Basic.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: 'Basic '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header authorization Bearer sin un jwt.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: 'Vearer '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 400 cuando manda un header authorization Bearer con un jwt invalido.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: 'Bearer abcdef'
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /pacientes/{paciente} responde 401 cuando manda un header authorization Bearer con un jwt falso.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/pacientes/1',
            headers: {
                authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1QHNwby5vZiJ9.XjEcNMNRB5D9vbrblGY_JJIBAhdenTM3_qH5uRFeoPs'
            }
        });

        expect(u.statusCode).to.equal(401);
    });

});
