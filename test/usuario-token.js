'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Familia de pruebas unitarias para manejar el token a [/usuarios/{usuario}] con header authorization Bearer.', () => {

    let server;

    let usuarioDoctor;

    let usuarioAdmin;

    it('[GET] /usuarios/{usuario} responde 200 cuando el rol de {usuario} es igual [Administrador] .', async () => {

        server = await Server.deployment();

        await server.models()
            .Usuario
            .query()
            .delete();

        usuarioDoctor = await server.models()
            .Usuario
            .query()
            .insert({
                nombre: 'Shaun',
                apellido: 'Murphy',
                correo: 'good.doctor@email.com',
                contraseña: 'paracetamol',
                rol: 'medico'
            });

        usuarioAdmin = await server.models()
            .Usuario
            .query()
            .insert({
                nombre: 'Elliot',
                apellido: 'Alderson',
                correo: 'elliot@email.com',
                contraseña: 'mr.robot',
                rol: 'administrador'
            });

        const token = Jwt.sign({ sub: usuarioAdmin.id, scope: [usuarioAdmin.rol] }, 'allsafe');

        const u = await server.inject({
            method: 'get',
            url: `/usuarios/${usuarioAdmin.id}`,
            headers: {
                authorization: `Bearer ${token}`
            }
        });

        expect(u.statusCode).to.equal(200);

    });

    it('[GET] /usuarios/{usuario} responde 200 cuando solicita su perfil {usuario}.', async () => {

        const token = Jwt.sign({ sub: usuarioDoctor.id, scope: [usuarioDoctor.rol] }, 'allsafe');

        const u = await server.inject({
            method: 'get',
            url: `/usuarios/${usuarioDoctor.id}`,
            headers: {
                authorization: `Bearer ${token}`
            }
        });

        expect(u.statusCode).to.equal(200);

    });

    it('[GET] /usuarios/{usuario} responde 400 cuando {usuario} es un id igual a 0.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/0'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando {usuario} es un id negativo.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/-22'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando no manda ningun header.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1'
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header vacio.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {}
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header authorization vacio.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: ''
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header authorization Bearer sin un jwt.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: 'Bearer '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header authorization Basic.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: 'Basic '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header authorization Bearer (incorrecto) sin un jwt.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: 'Vearer '
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 400 cuando manda un header authorization Bearer con un jwt invalido.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: 'Bearer abcdef'
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 401 cuando manda un header authorization Bearer con un jwt falso.', async () => {

        const u = await server.inject({
            method: 'get',
            url: '/usuarios/1',
            headers: {
                authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1QHNwby5vZiJ9.XjEcNMNRB5D9vbrblGY_JJIBAhdenTM3_qH5uRFeoPs'
            }
        });

        expect(u.statusCode).to.equal(401);
    });

    it('[GET] /usuarios/{usuario} responde 403 cuando el {usuario} pide a otro usuario.', async () => {

        const token = Jwt.sign({ sub: usuarioDoctor.id, scope: [usuarioDoctor.rol] }, 'allsafe');

        const u = await server.inject({
            method: 'get',
            url: `/usuarios/${usuarioAdmin.id}`,
            headers: {
                authorization: `Bearer ${token}`
            }
        });

        expect(u.statusCode).to.equal(403);

        await server.models()
            .Usuario
            .query()
            .delete();

    });

});
