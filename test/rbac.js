'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Familia de pruebas unitarias para RBAC codigo 200.', () => {

    let citaId;

    let pacienteId;

    let server;

    const tokenA = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    const tokenS = Jwt.sign({ sub: 2, scope: ['secretaria'] }, 'allsafe');

    const tokenE = Jwt.sign({ sub: 3, scope: ['enfermera'] }, 'allsafe');

    let usuarioId;
    //  Prueba para usuarios con token de administrador.
    it('[POST] /usuarios admin realiza alta de un usuario. ', async () => {

        server = await Server.deployment();

        await server.models()
            .Usuario
            .query()
            .delete();

        const usuario = await server.inject({
            method: 'post',
            url: '/usuarios',
            payload: {
                nombre: 'Antonio',
                apellido: 'Post',
                correo: 'antonio_b13@email.com',
                contraseña: 'contraseña',
                rol: 'medico'
            },
            headers: { authorization: `Bearer ${tokenA}` }
        });

        usuarioId = usuario.result.id;

        expect(usuario.statusCode).to.equal(200);
        expect(usuario.result).to.be.an.object();
        expect(usuario.result.id).to.be.a.number();
        expect(usuario.result.nombre).to.be.a.string();
        expect(usuario.result.apellido).to.be.a.string();
        expect(usuario.result.correo).to.be.a.string();
        expect(usuario.result.contraseña).to.not.exist();
        expect(usuario.result.rol).to.be.a.string();

    });

    it('[GET] /usuarios admin solicita la lista de usuarios.', async () => {

        const usuarios = await server.inject({
            method: 'get',
            url: '/usuarios',
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(usuarios.statusCode).to.equal(200);
        expect(usuarios.result).to.be.array();
        expect(usuarios.result).to.not.be.empty();
        for (const usuario of usuarios.result) {
            expect(usuario).to.be.an.object();
            expect(usuario.id).to.be.a.number();
            expect(usuario.id).to.be.above(0);

            expect(usuario.nombre).to.be.a.string();
            expect(usuario.apellido).to.be.a.string();
            expect(usuario.correo).to.be.a.string();
            expect(usuario.contraseña).to.not.exist();
            expect(usuario.rol).to.be.a.string();
        }

    });

    it('[GET] /usuarios/{usuario} admin solicita un usuario en particular.', async () => {

        const usuarios = await server.inject({
            method: 'get',
            url: `/usuarios/${usuarioId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(usuarios.statusCode).to.equal(200);
        const usuario = usuarios.result;

        expect(usuario).to.be.an.object();
        expect(usuario.id).to.be.a.number();
        expect(usuario.id).to.be.equal(usuarioId);
        expect(usuario.nombre).to.be.a.string();
        expect(usuario.apellido).to.be.a.string();
        expect(usuario.correo).to.be.a.string();
        expect(usuario.contraseña).to.not.exist();
        expect(usuario.rol).to.be.a.string();

    });

    it('[PATCH] /usuarios/{usuario} admin actualiza un usuario.', async () => {

        const usuario = await server.inject({
            method: 'patch',
            url: `/usuarios/${usuarioId}`,
            headers: { authorization: `Bearer ${tokenA}` },
            payload: {
                nombre: 'Antonio',
                apellido: 'Actualizado',
                correo: 'elliot@ecorp.com',
                contraseña: 'ñiño,ñiña',
                rol: 'Doctor'
            }
        });

        expect(usuario.statusCode).to.equal(200);
        expect(usuario.result).to.be.an.object();
        expect(usuario.result.id).to.be.a.number();
        expect(usuario.result.nombre).to.be.a.string();
        expect(usuario.result.apellido).to.be.a.string();
        expect(usuario.result.correo).to.be.a.string();
        expect(usuario.result.contraseña).to.not.exist();
        expect(usuario.result.rol).to.be.a.string();

    });

    it('[DELETE] /usuarios/{usuario} admin elimina un usuario.', async () => {

        const usuario = await server.inject({
            method: 'delete',
            url: `/usuarios/${usuarioId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        const usuarioDelete = await server.models()
            .Usuario
            .query()
            .where({ id: usuarioId })
            .delete();

        expect(usuario.statusCode).to.equal(200);
        expect(usuarioDelete).to.equal(0);

        await server.models()
            .Usuario
            .query()
            .delete();

    });
    // Prueba para pacientes con token de administrador.
    it('[POST] /pacientes admin realiza alta de un paciente. ', async () => {

        await server.models()
            .Paciente
            .query()
            .delete();

        const paciente = await server.inject({
            method: 'post',
            url: '/pacientes',
            payload: {
                nombre: 'Antonio',
                apellido: 'Post',
                telefono: 9988776655,
                correo: 'antonio.b13@ecorp.com',
                rfc: 'XYZW0987654X9'
            },
            headers: { authorization: `Bearer ${tokenA}` }
        });

        pacienteId = paciente.result.id;

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

    });

    it('[GET] /pacientes admin solicita la lista de pacientes.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: '/pacientes',
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        expect(pacientes.result).to.be.array();
        expect(pacientes.result).to.not.be.empty();
        for (const paciente of pacientes.result) {
            expect(paciente).to.be.an.object();
            expect(paciente.id).to.be.a.number();
            expect(paciente.id).to.be.above(0);

            expect(paciente.nombre).to.be.a.string();
            expect(paciente.apellido).to.be.a.string();
            expect(paciente.telefono).to.be.a.number();
        }

    });

    it('[GET] /pacientes/{paciente} admin solicita a un paciente en especifico.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        const paciente = pacientes.result;

        expect(paciente).to.be.an.object();
        expect(paciente.id).to.be.a.number();
        expect(paciente.id).to.equal(pacienteId);
        expect(paciente.nombre).to.be.a.string();
        expect(paciente.apellido).to.be.a.string();
        expect(paciente.rfc).to.be.a.string();

    });

    it('[PATCH] /pacientes/{paciente} admin actualiza un paciente.', async () => {

        const paciente = await server.inject({
            method: 'patch',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${tokenA}` },
            payload: {
                nombre: 'Antonio',
                apellido: 'Patch',
                telefono: 2729765312,
                correo: 'actualizacion@email.com',
                rfc: 'NUMB6543219X8'
            }
        });

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

    });

    it('[DELETE] /pacientes/{paciente} admin elimina un usuario.', async () => {

        const paciente = await server.inject({
            method: 'delete',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        const pacienteDelete = await server.models()
            .Paciente
            .query()
            .where({ id: pacienteId })
            .delete();

        expect(paciente.statusCode).to.equal(200);
        expect(pacienteDelete).to.equal(0);

        await server.models()
            .Paciente
            .query()
            .delete();

    });
    // Prueba para citas con token de administrador.
    it('[POST] /citas admin realiza alta de una cita. ', async () => {

        await server.models()
            .Cita
            .query()
            .delete();

        const cita = await server.inject({
            method: 'post',
            url: '/citas',
            payload: {
                paciente: 1,
                medico: 2,
                inicio: 1565017200000,
                fin: 1565019000000
            },
            headers: { authorization: `Bearer ${tokenA}` }
        });

        citaId = cita.result.id;

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();

    });

    it('[GET] /citas admin solicita la lista de citas.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: '/citas',
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(citas.statusCode).to.equal(200);
        expect(citas.result).to.be.array();
        expect(citas.result).to.not.be.empty();
        for (const cita of citas.result) {
            expect(cita).to.be.an.object();
            expect(cita.id).to.be.a.number();
            expect(cita.id).to.be.above(0);

            expect(cita.paciente).to.be.a.number();
            expect(cita.medico).to.be.a.number();
        }

    });

    it('[GET] /citas/{cita} admin solicita a una cita en especifico.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        expect(citas.statusCode).to.equal(200);
        const cita = citas.result;

        expect(cita).to.be.an.object();
        expect(cita.id).to.be.a.number();
        expect(cita.id).to.equal(citaId);
        expect(cita.paciente).to.be.a.number();
        expect(cita.medico).to.be.a.number();
        expect(cita.inicio).to.be.a.number();
        expect(cita.fin).to.be.a.number();

    });

    it('[PATCH] /citas/{cita} admin actualiza una cita.', async () => {

        const cita = await server.inject({
            method: 'patch',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenA}` },
            payload: {
                paciente: 22,
                medico: 2,
                inicio: 1570438800000,
                fin: 1570442400000
            }
        });

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();
    });


    it('[DELETE] /citas/{cita} admin elimina una cita.', async () => {

        const cita = await server.inject({
            method: 'delete',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenA}` }
        });

        const deleted = await server.models()
            .Cita
            .query()
            .where({ id: citaId })
            .delete();

        expect(cita.statusCode).to.equal(200);
        expect(deleted).to.equal(0);

        await server.models()
            .Cita
            .query()
            .delete();

    });
    // Prueba para pacientes con token de secretaria.
    it('[POST] /pacientes secretaria realiza alta de un paciente. ', async () => {

        await server.models()
            .Paciente
            .query()
            .delete();

        const paciente = await server.inject({
            method: 'post',
            url: '/pacientes',
            payload: {
                nombre: 'Antonio',
                apellido: 'Post',
                telefono: 9988776655,
                correo: 'antonio.b13@ecorp.com',
                rfc: 'XYZW0987654X9'
            },
            headers: { authorization: `Bearer ${tokenS}` }
        });

        pacienteId = paciente.result.id;

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

    });

    it('[GET] /pacientes secretaria solicita la lista de pacientes.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: '/pacientes',
            headers: { authorization: `Bearer ${tokenS}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        expect(pacientes.result).to.be.array();
        expect(pacientes.result).to.not.be.empty();
        for (const paciente of pacientes.result) {
            expect(paciente).to.be.an.object();
            expect(paciente.id).to.be.a.number();
            expect(paciente.id).to.be.above(0);

            expect(paciente.nombre).to.be.a.string();
            expect(paciente.apellido).to.be.a.string();
            expect(paciente.telefono).to.be.a.number();
        }

    });

    it('[GET] /pacientes/{paciente} secretaria solicita a un paciente en especifico.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${tokenS}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        const paciente = pacientes.result;

        expect(paciente).to.be.an.object();
        expect(paciente.id).to.be.a.number();
        expect(paciente.id).to.equal(pacienteId);
        expect(paciente.nombre).to.be.a.string();
        expect(paciente.apellido).to.be.a.string();
        expect(paciente.rfc).to.be.a.string();

    });

    it('[PATCH] /pacientes/{paciente} secretaria actualiza un paciente.', async () => {

        const paciente = await server.inject({
            method: 'patch',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${tokenS}` },
            payload: {
                nombre: 'Antonio',
                apellido: 'Patch',
                telefono: 2729765312,
                correo: 'actualizacion@email.com',
                rfc: 'NUMB6543219X8'
            }
        });

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

        await server.models()
            .Paciente
            .query()
            .delete();

    });
    // Prueba para citas con token de administrador.
    it('[POST] /citas secretaria realiza alta de una cita. ', async () => {

        await server.models()
            .Cita
            .query()
            .delete();

        const cita = await server.inject({
            method: 'post',
            url: '/citas',
            payload: {
                paciente: 1,
                medico: 2,
                inicio: 1565017200000,
                fin: 1565019000000
            },
            headers: { authorization: `Bearer ${tokenS}` }
        });

        citaId = cita.result.id;

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();

    });

    it('[GET] /citas secretaria solicita la lista de citas.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: '/citas',
            headers: { authorization: `Bearer ${tokenS}` }
        });

        expect(citas.statusCode).to.equal(200);
        expect(citas.result).to.be.array();
        expect(citas.result).to.not.be.empty();
        for (const cita of citas.result) {
            expect(cita).to.be.an.object();
            expect(cita.id).to.be.a.number();
            expect(cita.id).to.be.above(0);

            expect(cita.paciente).to.be.a.number();
            expect(cita.medico).to.be.a.number();
        }

    });

    it('[GET] /citas/{cita} secretaria solicita a una cita en especifico.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenS}` }
        });

        expect(citas.statusCode).to.equal(200);
        const cita = citas.result;

        expect(cita).to.be.an.object();
        expect(cita.id).to.be.a.number();
        expect(cita.id).to.equal(citaId);
        expect(cita.paciente).to.be.a.number();
        expect(cita.medico).to.be.a.number();
        expect(cita.inicio).to.be.a.number();
        expect(cita.fin).to.be.a.number();

    });

    it('[PATCH] /citas/{cita} secretaria actualiza una cita.', async () => {

        const cita = await server.inject({
            method: 'patch',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenS}` },
            payload: {
                paciente: 22,
                medico: 2,
                inicio: 1570438800000,
                fin: 1570442400000
            }
        });

        expect(cita.statusCode).to.equal(200);
        expect(cita.result).to.be.an.object();
        expect(cita.result.id).to.be.a.number();
        expect(cita.result.paciente).to.be.a.number();
        expect(cita.result.medico).to.be.a.number();
        expect(cita.result.inicio).to.be.a.number();
        expect(cita.result.fin).to.be.a.number();

    });
    // Prueba para citas con token de enfermera.
    it('[GET] /citas enfermera solicita la lista de citas.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: '/citas',
            headers: { authorization: `Bearer ${tokenE}` }
        });

        expect(citas.statusCode).to.equal(200);
        expect(citas.result).to.be.array();
        expect(citas.result).to.not.be.empty();
        for (const cita of citas.result) {
            expect(cita).to.be.an.object();
            expect(cita.id).to.be.a.number();
            expect(cita.id).to.be.above(0);

            expect(cita.paciente).to.be.a.number();
            expect(cita.medico).to.be.a.number();
        }

    });

    it('[GET] /citas/{cita} enfermera solicita a una cita en especifico.', async () => {

        const citas = await server.inject({
            method: 'get',
            url: `/citas/${citaId}`,
            headers: { authorization: `Bearer ${tokenE}` }
        });

        expect(citas.statusCode).to.equal(200);
        const cita = citas.result;

        expect(cita).to.be.an.object();
        expect(cita.id).to.be.a.number();
        expect(cita.id).to.equal(citaId);
        expect(cita.paciente).to.be.a.number();
        expect(cita.medico).to.be.a.number();
        expect(cita.inicio).to.be.a.number();
        expect(cita.fin).to.be.a.number();

        await server.models()
            .Cita
            .query()
            .delete();

    });

});
