'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

// Utils
const { randomBytes } = require('crypto');

/**
* Returns a random integer between min (inclusive) and max (inclusive).
* The value is no lower than min (or the next integer greater than min
* if min isn't an integer) and no greater than max (or the next integer
* lower than max if max isn't an integer).
* Using Math.round() will give you a non-uniform distribution!
*/
const randomInt = (min, max) => {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

describe('Familia de pruebas unitarias para errores 4xx en /usuarios/{usuario}', () => {

    const token = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    it('[GET] /usuarios/{usuario} responde 400 cuando {usuario} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} de 4 caracteres, cuidando que NO sea número positivo
        let usuario;
        do {
            usuario = randomBytes(4).toString('hex');
        }
        while (!isNaN(usuario) || Number(usuario) > 0);

        // Pido el {usuario}; NOTA: {usuario} no es válido por no ser número entero positivo
        const u = await server.inject({
            method: 'get',
            url: `/usuarios/${usuario}`
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[PATCH] /usuarios/{usuario} responde 400 cuando {usuario} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} de 4 caracteres, cuidando que NO sea número positivo
        let usuario;
        do {
            usuario = randomBytes(4).toString('hex');
        }
        while (!isNaN(usuario) || Number(usuario) > 0);

        // Pido el {usuario}; NOTA: {usuario} no es válido por no ser número entero positivo
        const u = await server.inject({
            method: 'patch',
            url: `/usuarios/${usuario}`,
            payload: {
                apellido: 'Suárez'
            }
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[DELETE] /usuarios/{usuario} responde 400 cuando {usuario} no es un id válido', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} de 4 caracteres, cuidando que NO sea número positivo
        let usuario;
        do {
            usuario = randomBytes(4).toString('hex');
        }
        while (!isNaN(usuario) || Number(usuario) > 0);

        // Pido el {usuario}; NOTA: {usuario} no es válido por no ser número entero positivo
        const u = await server.inject({
            method: 'delete',
            url: `/usuarios/${usuario}`
        });

        expect(u.statusCode).to.equal(400);
    });

    it('[GET] /usuarios/{usuario} responde 404 cuando {usuario} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} muy grande; tan grande que no está en la BD
        const usuario = randomInt(9000000, 1000000);
        // Pido el {usuario}; NOTA: {usuario} sí es válido, pero no existe
        const u = await server.inject({
            method: 'get',
            url: `/usuarios/${usuario}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(u.statusCode).to.equal(404);
    });

    it('[PATCH] /usuarios/{usuario} responde 404 cuando {usuario} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} muy grande; tan grande que no está en la BD
        const usuario = randomInt(9000000, 1000000);
        // Pido todas las citas de {usuario}; NOTA: {usuario} sí es válido, pero no existe
        const u = await server.inject({
            method: 'patch',
            url: `/usuarios/${usuario}`,
            headers: { authorization: `Bearer ${token}` },
            payload: {
                apellido: 'Suárez'
            }
        });

        expect(u.statusCode).to.equal(404);
    });

    it('[DELETE] /usuarios/{usuario} responde 404 cuando {usuario} no existe en la BD', async () => {

        const server = await Server.deployment();

        // Genero un id {usuario} muy grande; tan grande que no está en la BD
        const usuario = randomInt(9000000, 1000000);
        // Pido todas las citas de {usuario}; NOTA: {usuario} sí es válido, pero no existe
        const u = await server.inject({
            method: 'delete',
            url: `/usuarios/${usuario}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(u.statusCode).to.equal(404);
    });
});
