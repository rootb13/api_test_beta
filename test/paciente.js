'use strict';

// Load modules

const Jwt = require('jsonwebtoken');
const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('Pruebas para Pacientes', () => {

    let pacienteId;

    let server;

    const token = Jwt.sign({ sub: 1, scope: ['admin'] }, 'allsafe');

    it('[POST] /pacientes.', async () => {

        server = await Server.deployment();

        await server.models()
            .Paciente
            .query()
            .delete();

        const paciente = await server.inject({
            method: 'post',
            url: '/pacientes',
            payload: {
                nombre: 'Antonio',
                apellido: 'Post',
                telefono: 5566778899,
                correo: 'alderson@ecorp.com',
                rfc: 'ABCD0123459X9'
            },
            headers: { authorization: `Bearer ${token}` }
        });

        pacienteId = paciente.result.id;

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

    });

    it('[POST] /pacientes no admite correos duplicados', async () => {

        const correoDuplicado = await server.inject({
            method: 'post',
            url: '/pacientes',
            payload: {
                nombre: 'Antonio',
                apellido: 'Correo Duplicado',
                telefono: 5511223377,
                correo: 'alderson@ecorp.com',
                rfc: 'ZXYW9876540X0'
            }
        });

        expect(correoDuplicado.statusCode).to.equal(400);

    });

    it('[GET] /pacientes/{paciente} funciona.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        const paciente = pacientes.result;

        expect(paciente).to.be.an.object();
        expect(paciente.id).to.be.a.number();
        expect(paciente.id).to.equal(pacienteId);
        expect(paciente.nombre).to.be.a.string();
        expect(paciente.apellido).to.be.a.string();
        expect(paciente.rfc).to.be.a.string();

    });

    it('[GET] /pacientes funciona.', async () => {

        const pacientes = await server.inject({
            method: 'get',
            url: '/pacientes',
            headers: { authorization: `Bearer ${token}` }
        });

        expect(pacientes.statusCode).to.equal(200);
        expect(pacientes.result).to.be.array();
        expect(pacientes.result).to.not.be.empty();
        for (const paciente of pacientes.result) {
            expect(paciente).to.be.an.object();
            expect(paciente.id).to.be.a.number();
            expect(paciente.id).to.be.above(0);

            expect(paciente.nombre).to.be.a.string();
            expect(paciente.apellido).to.be.a.string();
            expect(paciente.telefono).to.be.a.number();
        }

    });

    it('[PATCH] /{paciente}.', async () => {

        const paciente = await server.inject({
            method: 'patch',
            url: `/pacientes/${pacienteId}`,
            payload: {
                nombre: 'Antonio',
                apellido: 'Patch',
                telefono: 9987654321,
                correo: 'correo@pruebas.com',
                rfc: 'ABCD1234560X1'
            },
            headers: { authorization: `Bearer ${token}` }
        });

        expect(paciente.statusCode).to.equal(200);
        expect(paciente.result).to.be.an.object();
        expect(paciente.result.id).to.be.a.number();
        expect(paciente.result.nombre).to.be.a.string();
        expect(paciente.result.apellido).to.be.a.string();
        expect(paciente.result.telefono).to.be.a.number();
        expect(paciente.result.correo).to.be.a.string();
        expect(paciente.result.rfc).to.be.a.string();

    });

    it('[DELETE] /{paciente}.', async () => {

        const paciente = await server.inject({
            method: 'delete',
            url: `/pacientes/${pacienteId}`,
            headers: { authorization: `Bearer ${token}` }
        });

        const pacienteDelete = await server.models()
            .Paciente
            .query()
            .where({ id: pacienteId })
            .delete();

        expect(paciente.statusCode).to.equal(200);
        expect(pacienteDelete).to.equal(0);

        await server.models()
            .Paciente
            .query()
            .delete();

    });

});
