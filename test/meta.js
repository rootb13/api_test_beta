'use strict';

// Load modules

const Code = require('@hapi/code');
const Lab = require('@hapi/lab');
const Server = require('../server');

// Test shortcuts

const { describe, it } = exports.lab = Lab.script();
const { expect } = Code;

describe('endpoints con información sobre el api mismo', () => {

    it('[GET] /version está disponible ', async () => {

        const server = await Server.deployment();

        const version = await server.inject({
            method: 'get',
            url: '/version'
        });

        expect(version.statusCode).to.equal(200);
        expect(version.result).to.be.an.object();
        expect(version.result.author).to.be.a.string();
    });
});
